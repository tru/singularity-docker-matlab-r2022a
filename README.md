# building a matlab R2022a toy system for singularity and docker image for CI

Tru <tru@pasteur.fr>

## Why ?
- toy system for gitlab-CI
- build docker image from dockerhub registry and push to registry.pasteur.fr with proper tags
- build docker image, push to registry.pasteur.fr and re-use that docker image to create a singularity container as artefact
- keeping an onsite version of https://hub.docker.com/r/mathworks/matlab

## Caveat
- playground, use at your own risk!
- `:main` tagged docker image
- `:latest` tagged singularity image

## Usage:

Docker: cf https://hub.docker.com/r/mathworks/matlab
```
docker run -e MLM_LICENSE_FILE=27000@MyLicenseServer -ti registry-gitlab.pasteur.fr/tru/singularity-docker-matlab-r2022a:main
```
Singularity:
```
singularity run  --env MLM_LICENSE_FILE=27000@MyLicenseServer oras://registry-gitlab.pasteur.fr/tru/singularity-docker-matlab-r2022a:latest
```
